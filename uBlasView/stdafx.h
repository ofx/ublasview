#pragma once

#include <Windows.h>

#include <stdio.h>
#include <string>
#include <iostream>
#include <math.h>
#include <stdlib.h>
#include <fstream>
#include <sstream>

#include <GL\glew.h>
#include <GL\glut.h>

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <glm/gtc/type_ptr.hpp>

#include <boost/numeric/ublas/vector.hpp>
#include <boost/numeric/ublas/matrix.hpp>
#include <boost/numeric/ublas/matrix_proxy.hpp>
#include <boost/numeric/ublas/io.hpp>
#include <boost/foreach.hpp>

#if _WIN32
	#include <direct.h>
	#define getcwd _getcwd
#endif