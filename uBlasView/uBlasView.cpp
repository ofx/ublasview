#include "stdafx.h"

#include "ShaderUtils.h"

GLuint g_Program;

GLint g_AttributeCoord2d;
GLint g_UniformVertexTransform;
GLint g_UniformTextureTransform;
GLuint g_TextureId;

GLint g_UniformTexture;

float g_OffsetX = 0.0;
float g_OffsetY = 0.0;
float g_Scale   = 1.0;

GLuint g_View = GL_LINES;

glm::mat4 g_Model(1.0f);

bool g_Interpolate = false;
bool g_Clamp       = false;
bool g_Rotate      = false;

GLuint g_Vbo[2];

typedef float Value;

typedef boost::numeric::ublas::matrix<Value> Matrix;

Matrix g_Matrix;

bool InitResources(void)
{
	g_Program = create_program("ublasview.vs", "ublasview.fs");
	if (g_Program == 0)
	{
		return 0;
	}

	g_AttributeCoord2d		  = get_attrib(g_Program, "coord2d");
	g_UniformVertexTransform  = get_uniform(g_Program, "vertex_transform");
	g_UniformTextureTransform = get_uniform(g_Program, "texture_transform");
	g_UniformTexture		  = get_uniform(g_Program, "mytexture");

	if (g_AttributeCoord2d == -1 || g_UniformVertexTransform == -1 || g_UniformTextureTransform == -1 || g_UniformTexture == -1)
	{
		return 0;
	}

	const unsigned int ii = g_Matrix.size1(), jj = g_Matrix.size2();
	GLbyte **graph = new GLbyte*[ii];
	for (int i = 0 ; i < ii ; ++i)
	{
		graph[i] = new GLbyte[jj];
	}

	for (int i = 0 ; i < ii ; ++i) 
	{
		for (int j = 0 ; j < jj ; ++j) 
		{
			graph[i][j] = g_Matrix(i, j) / 10.0;
		}
	}

	glActiveTexture(GL_TEXTURE0);
	glGenTextures(1, &g_TextureId);
	glBindTexture(GL_TEXTURE_2D, g_TextureId);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_LUMINANCE, jj, ii, 0, GL_LUMINANCE, GL_UNSIGNED_BYTE, graph);

	glGenBuffers(2, g_Vbo);

	glm::vec2 vertices[202][202];

	for (int i = 0 ; i < 202 ; ++i)
	{
		for (int j = 0 ; j < 202 ; ++j) 
		{
			vertices[i][j].x = (j - 100) / 100.0;
			vertices[i][j].y = (i - 100) / 100.0;
		}
	}

	glBindBuffer(GL_ARRAY_BUFFER, g_Vbo[0]);
	glBufferData(GL_ARRAY_BUFFER, sizeof vertices, vertices, GL_STATIC_DRAW);

	GLushort indices[200 * 202 * 4];
	int i = 0;

	for (int y = 0 ; y < 202 ; ++y) 
	{
		for (int x = 0 ; x < 200 ; ++x) 
		{
			indices[i++] = y * 202 + x;
			indices[i++] = y * 202 + x + 1;
		}
	}

	for (int x = 0 ; x < 202 ; ++x) 
	{
		for (int y = 0 ; y < 200 ; ++y) 
		{
			indices[i++] = y * 202 + x;
			indices[i++] = (y + 1) * 202 + x;
		}
	}

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_Vbo[1]);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof indices, indices, GL_STATIC_DRAW);

	return true;
}

bool FreeResources(void)
{
	glDeleteProgram(g_Program);

	return true;
}


void Display(void) 
{
	glUseProgram(g_Program);
	glUniform1i(g_UniformTexture, 0);

	if (g_Rotate)
	{
		g_Model = glm::rotate(glm::mat4(1.0f), float (glutGet(GLUT_ELAPSED_TIME) / 100.0), glm::vec3(0.0f, 0.0f, 1.0f));
	}

	glm::mat4 view = glm::lookAt(glm::vec3(0.0, -2.0, 2.0), glm::vec3(0.0, 0.0, 0.0), glm::vec3(0.0, 0.0, 1.0));
	glm::mat4 projection = glm::perspective(45.0f, 1.0f * 640 / 480, 0.1f, 10.0f);

	glm::mat4 vertex_transform = projection * view * g_Model;
	glm::mat4 texture_transform = glm::translate(glm::scale(glm::mat4(1.0f), glm::vec3(g_Scale, g_Scale, 1)), glm::vec3(g_OffsetX, g_OffsetY, 0));

	glUniformMatrix4fv(g_UniformVertexTransform, 1, GL_FALSE, glm::value_ptr(vertex_transform));
	glUniformMatrix4fv(g_UniformTextureTransform, 1, GL_FALSE, glm::value_ptr(texture_transform));

	glClearColor(0.0, 0.0, 0.0, 0.0);
	glClear(GL_COLOR_BUFFER_BIT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, g_Clamp ? GL_CLAMP_TO_EDGE : GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, g_Clamp ? GL_CLAMP_TO_EDGE : GL_REPEAT);

	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, g_Interpolate ? GL_LINEAR : GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, g_Interpolate ? GL_LINEAR : GL_NEAREST);

	glEnableVertexAttribArray(g_AttributeCoord2d);

	glBindBuffer(GL_ARRAY_BUFFER, g_Vbo[0]);
	glVertexAttribPointer(g_AttributeCoord2d, 2, GL_FLOAT, GL_FALSE, 0, 0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, g_Vbo[1]);
	glDrawElements(g_View, 200 * 202 * 4, GL_UNSIGNED_SHORT, 0);

	glDisableVertexAttribArray(g_AttributeCoord2d);
	glBindBuffer(GL_ARRAY_BUFFER, 0);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);

	glutSwapBuffers();
}

void Special(int key, int x, int y) 
{
	switch (key) 
	{
	case GLUT_KEY_F1:
		g_Interpolate = !g_Interpolate;
		break;
	case GLUT_KEY_F2:
		g_Clamp = !g_Clamp;
		break;
	case GLUT_KEY_F3:
		g_Rotate = !g_Rotate;
		break;
	case GLUT_KEY_LEFT:
		g_OffsetX -= 0.03;
		break;
	case GLUT_KEY_RIGHT:
		g_OffsetX += 0.03;
		break;
	case GLUT_KEY_UP:
		g_OffsetY += 0.03;
		break;
	case GLUT_KEY_DOWN:
		g_OffsetY -= 0.03;
		break;
	case GLUT_KEY_PAGE_UP:
		g_Scale *= 1.5;
		break;
	case GLUT_KEY_PAGE_DOWN:
		g_Scale /= 1.5;
		break;
	case GLUT_KEY_F4:
		g_View = g_View == GL_LINES ? GL_POINTS : GL_LINES;
		break;
	case GLUT_KEY_HOME:
		g_OffsetX = 0.0;
		g_OffsetY = 0.0;
		g_Scale = 1.0;
		break;
	}

	glutPostRedisplay();
}

bool InitGL(int argc, char**argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGB | GLUT_DOUBLE);
	glutInitWindowSize(640, 480);
	glutCreateWindow("uBlasView");

	GLenum glew_status = glewInit();

	if (GLEW_OK != glew_status) 
	{
		std::cerr << "Could not initialize GL: " << glewGetErrorString(glew_status) << std::endl;
		return false;
	}

	if (!GLEW_VERSION_2_0) 
	{
		std::cerr << "Support for OpenGL 2.0 is required, not found" << std::endl;
		return false;
	}

	GLint max_units;

	glGetIntegerv(GL_MAX_VERTEX_TEXTURE_IMAGE_UNITS, &max_units);
	if (max_units < 1) 
	{
		std::cerr << "GPU does not have any vertex texture image unites" << std::endl;
		return 1;
	}

	if (InitResources()) 
	{
		glutDisplayFunc(Display);
		glutIdleFunc(Display);
		glutSpecialFunc(Special);
		glutMainLoop();
	}

	FreeResources();
	return 0;
}

bool LoadMatrix(std::string file)
{
	std::ifstream s(file);
	if (s.good())
	{
		std::string header;
		s >> g_Matrix;
		s.close();
	}
	else
	{
		std::cerr << "Matrix file not found" << std::endl;
		return false;
	}

	return true;
}

int main(int argc, char** argv)
{
	// Expect the ublas matrix file as the first parameter, if not present, die
	/*if (argc == 1)
	{
		std::cerr << "Usage: " << argv[0] << " <ublas-matrix-file>" << std::endl << "\tNo uBlas Matrix file was specified" << std::endl;
		return 1;
	}*/

	// Define the matrix file
	std::string matrixFile/* = argv[1]*/;

	matrixFile = "C:\\Users\\marlon\\My Projects\\openclonk\\cl\\data\\matrix.csv";

	if (LoadMatrix(matrixFile))
	{
		InitGL(argc, argv);
	}

	return 0;
}

